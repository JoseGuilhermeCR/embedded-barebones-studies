#ifndef RCC_H_
#define RCC_H_

#include "utils.h"

#if defined(__cplusplus)
extern "C" {
#endif

enum rcc_ahb1_peripheral
{
    RCC_AHB1_PERIPHERAL_GPIO_A    = 0x001,
    RCC_AHB1_PERIPHERAL_GPIO_B    = 0x002,
    RCC_AHB1_PERIPHERAL_GPIO_C    = 0x004,
    RCC_AHB1_PERIPHERAL_GPIO_D    = 0x008,
    RCC_AHB1_PERIPHERAL_GPIO_E    = 0x010,
    RCC_AHB1_PERIPHERAL_GPIO_F    = 0x020,
    RCC_AHB1_PERIPHERAL_GPIO_G    = 0x040,
    RCC_AHB1_PERIPHERAL_GPIO_H    = 0x080,
    RCC_AHB1_PERIPHERAL_GPIO_I    = 0x100,
    RCC_AHB1_PERIPHERAL_GPIO_J    = 0x200,
    RCC_AHB1_PERIPHERAL_GPIO_K    = 0x400,
    RCC_AHB1_PERIPHERAL_CRC       = 0x1000,
    RCC_AHB1_PERIPHERAL_BKPSRAM   = 0x40000,
    RCC_AHB1_PERIPHERAL_DTCMRAM   = 0x100000,
    RCC_AHB1_PERIPHERAL_DMA1      = 0x200000,
    RCC_AHB1_PERIPHERAL_DMA2      = 0x400000,
    RCC_AHB1_PERIPHERAL_DMA2D     = 0x800000,
    RCC_AHB1_PERIPHERAL_ETHMAC    = 0x2000000,
    RCC_AHB1_PERIPHERAL_ETHMACTX  = 0x4000000,
    RCC_AHB1_PERIPHERAL_ETHMACRX  = 0x8000000,
    RCC_AHB1_PERIPHERAL_ETHMACPTP = 0x10000000,
    RCC_AHB1_PERIPHERAL_OTGHSEN   = 0x20000000,
    RCC_AHB1_PERIPHERAL_OTGHSULPI = 0x40000000,
};

struct rcc {
    volatile u32 cr;
    volatile u32 pllcfgr;
    volatile u32 cfgr;
    volatile u32 cir;
    volatile u32 ahb1rstr;
    volatile u32 ahb2rstr;
    volatile u32 ahb3rstr;

    volatile u32 reserved_0;

    volatile u32 apb1rstr;
    volatile u32 apb2rstr;

    volatile u32 reserved_1;
    volatile u32 reserved_2;

    volatile u32 ahb1enr;
    volatile u32 ahb2enr;
    volatile u32 ahb3enr;

    volatile u32 reserved_4;

    volatile u32 apb1enr;
    volatile u32 apb2enr;

    volatile u32 reserved_5;
    volatile u32 reserved_6;

    volatile u32 ahb1lpenr;
    volatile u32 ahb2lpenr;
    volatile u32 ahb3lpenr;

    volatile u32 reserved_7;

    volatile u32 apb1lpenr;
    volatile u32 apb2lpenr;

    volatile u32 reserved_8;
    volatile u32 reserved_9;

    volatile u32 bdcr;
    volatile u32 csr;

    volatile u32 reserved_10;
    volatile u32 reserved_11;

    volatile u32 sscgr;
    volatile u32 plli2scfgr;
    volatile u32 pllsaicfgr;
    volatile u32 dckcfgr1;
    volatile u32 dckcfgr2;
};

#define RCC_REGISTERS_LENGTH ((u32)0x94)
_Static_assert(sizeof(struct rcc) == RCC_REGISTERS_LENGTH,
               "Structure is not big enough.");
#undef RCC_REGISTERS_LENGTH

extern i32 rcc_init(struct rcc **rcc);
extern u32 rcc_get_sys_tick();

#if defined(__cplusplus)
} // End of extern "C"
#endif

#endif
