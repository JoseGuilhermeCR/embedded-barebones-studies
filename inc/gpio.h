#ifndef GPIO_H_
#define GPIO_H_

#include "utils.h"

#if defined(__cplusplus)
extern "C" {
#endif

enum gpio_port
{
    GPIO_PORT_A = 0,
    GPIO_PORT_B,
    GPIO_PORT_C,
    GPIO_PORT_D,
    GPIO_PORT_E,
    GPIO_PORT_F,
    GPIO_PORT_G,
    GPIO_PORT_H,
    GPIO_PORT_I,
    GPIO_PORT_J,
    GPIO_PORT_K
};

enum gpio_mode
{
    GPIO_MODE_INPUT = 0,
    GPIO_MODE_OUTPUT,
    GPIO_MODE_ALTERNATE_FUNCTION,
    GPIO_MODE_ANALOG
};

struct gpio {
    volatile u32 moder;
    volatile u32 otyper;
    volatile u32 ospeedr;
    volatile u32 pupdr;
    volatile u32 idr;
    volatile u32 odr;
    volatile u32 bsrr;
    volatile u32 lckr;
    volatile u32 afrl;
    volatile u32 afrh;
};

#define GPIO_REGISTERS_LENGTH ((u32)0x28)
_Static_assert(sizeof(struct gpio) == GPIO_REGISTERS_LENGTH,
               "Structure is not big enough.");
#undef GPIO_REGISTERS_LENGTH

extern i32 gpio_init(struct gpio **gpio, enum gpio_port port);
extern i32 gpio_set_pin_mode(struct gpio *gpio, u8 pin, enum gpio_mode mode);
extern i32 gpio_set_pin_output(struct gpio *gpio, u8 pin, u8 gpio_output);
extern i32 gpio_get_pin_input(struct gpio *gpio, u8 pin, u8 *input);

#if defined(__cplusplus)
} // End of extern "C"
#endif

#endif
