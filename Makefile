OUT = main

ELF = $(OUT).elf
BIN = $(OUT).bin

SRC_PATH = src/
INC_PATH = inc/

C_SRC = $(wildcard $(SRC_PATH)*.c)
AS_SRC = $(wildcard $(SRC_PATH)*.S)

C_OBJ = $(C_SRC:.c=.co)
AS_OBJ = $(AS_SRC:.S=.aso)

DEP  = $(C_OBJ:.co=.d)
DEP += $(AS_OBJ:.aso=.d)

TOOLCHAIN_PREFIX = arm-none-eabi-

CC = $(TOOLCHAIN_PREFIX)gcc
GDB = $(TOOLCHAIN_PREFIX)gdb
OBJCOPY = $(TOOLCHAIN_PREFIX)objcopy

# Valor passado à mfpu foi retirado do "ARM Cortex-M7 Processor Technical Reference Manual".
# A cpu sendo usada tem precisão simples e dupla. Basta olhar na tabela o ISA suportado pela FPU.
CFLAGS += -mcpu=cortex-m7 -mthumb -mfpu=fpv5-d16 -mfloat-abi=hard
CFLAGS += -Wall -Wextra -Wno-main -Iinc/ -MMD -std=gnu11
CFLAGS += -static -fno-common

CFLAGS += -g3

LDFLAGS += -nostartfiles -nostdlib -lgcc
LDFLAGS += -Tlinker.ld

%.aso: %.S
	$(CC) $(CFLAGS) -c $< -o $@

%.co: %.c
	$(CC) $(CFLAGS) -c $< -o $@

$(OUT): $(C_OBJ) $(AS_OBJ)
	$(CC) $(LDFLAGS) $^ -o $(ELF)
	$(OBJCOPY) -O binary $(ELF) $(BIN)

# Regras de dependência geradas pelo compilador.
-include $(DEP)

# Para isso, openocd precisa estar rodando e conectado com a núcleo.
.PHONY: debug
debug: $(OUT)
	$(GDB) -ex "target extended-remote localhost:3333" $(ELF)

.PHONY: flash
flash: $(OUT)
	$(GDB) -ex "target extended-remote localhost:3333" -x flash_commands.gdb

.PHONY: openocd
openocd:
	openocd -f interface/stlink.cfg -f target/stm32f7x.cfg

.PHONY: clean
clean:
	rm -f $(DEP) $(C_OBJ) $(AS_OBJ) $(ELF) $(BIN)

.PHONY: cleandep
cleandep:
	rm -f $(DEP) $(C_OBJ) $(AS_OBJ)
