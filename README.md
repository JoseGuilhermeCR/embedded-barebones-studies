# Objective

This repository was created in order for me to put my studies about the cortex-m7 architecture.
I'll be using the STM32F67ZIT6U MCU that comes bundled with the NUCLEO-F767ZI board. The main objective
is to gain knowledge in assembly and linker usage, as well as using the resources from the architecture.

Since I'm dealing with an MCU and not only a CPU, I plan on making use of the peripherals that come
with it as well.
