#include "rcc.h"

#define RCC_BASE ((u32)0x40023800)

static u32 sys_tick_ms;

i32 rcc_init(struct rcc **rcc)
{
    if (!rcc) {
        return -1;
    }

    *rcc = (struct rcc *)RCC_BASE;

    return 0;
}

u32 rcc_get_sys_tick()
{
    return sys_tick_ms;
}

__attribute__((interrupt)) void sys_tick_handler()
{
    ++sys_tick_ms;
}
