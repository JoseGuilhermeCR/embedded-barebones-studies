#include "gpio.h"

#define GPIO_BASE             ((u32)0x40020000)
#define GPIO_MEMORY_AREA_SIZE ((u32)0x400)
#define GPIO_MAX_PIN          ((u8)15)

i32 gpio_init(struct gpio **gpio, enum gpio_port port)
{
    if (!gpio || port > GPIO_PORT_K) {
        return -1;
    }

    const u32 gpio_addr = GPIO_BASE + (GPIO_MEMORY_AREA_SIZE * (u32)port);
    *gpio               = (struct gpio *)gpio_addr;
    return 0;
}

i32 gpio_set_pin_mode(struct gpio *gpio, u8 pin, enum gpio_mode mode)
{
    if (!gpio || pin > GPIO_MAX_PIN || mode > GPIO_MODE_ANALOG) {
        return -1;
    }

    gpio->moder |= ((u8)mode << (pin * 2));
    return 0;
}

i32 gpio_set_pin_output(struct gpio *gpio, u8 pin, u8 gpio_output)
{
    if (!gpio || pin > GPIO_MAX_PIN) {
        return -1;
    }

    if (gpio_output) {
        gpio->bsrr = (0x00000001 << pin);
    } else {
        gpio->bsrr = (0x00010000 << pin);
    }
    return 0;
}

i32 gpio_get_pin_input(struct gpio *gpio, u8 pin, u8 *input)
{
    if (!gpio || pin > GPIO_MAX_PIN || !input) {
        return -1;
    }

    if ((gpio->idr & (1 << pin)) != 0) {
        *input = 1;
    } else {
        *input = 0;
    }
    return 0;
}
