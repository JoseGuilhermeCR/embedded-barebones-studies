#include "gpio.h"
#include "rcc.h"
#include "utils.h"

static struct rcc *rcc;
static struct gpio *gpio_b;

__attribute__((noreturn)) void main()
{
    rcc_init(&rcc);
    rcc->ahb1enr |= RCC_AHB1_PERIPHERAL_GPIO_B;

    gpio_init(&gpio_b, GPIO_PORT_B);
    gpio_set_pin_mode(gpio_b, 0, GPIO_MODE_OUTPUT);
    gpio_set_pin_mode(gpio_b, 7, GPIO_MODE_OUTPUT);
    gpio_set_pin_mode(gpio_b, 14, GPIO_MODE_OUTPUT);

    f64 test = 0.32 * 3.0;
    (void)test;

    // Começa ligado.
    gpio_set_pin_output(gpio_b, 0, 1);
    gpio_set_pin_output(gpio_b, 7, 1);
    gpio_set_pin_output(gpio_b, 14, 1);

    u32 start        = rcc_get_sys_tick();
    u8 on            = 0;
    u8 pin           = 0;
    u8 state         = 0;
    u8 state_counter = 0;

    while (1) {
        const u32 now = rcc_get_sys_tick();

        switch (state) {
            case 0:
                if (now >= start + 500) {
                    gpio_set_pin_output(gpio_b, 0, on);
                    gpio_set_pin_output(gpio_b, 7, on);
                    gpio_set_pin_output(gpio_b, 14, on);
                    on    = !on;
                    start = now;

                    ++state_counter;
                    if (state_counter >= 10) {
                        state         = 1;
                        state_counter = 0;
                        pin           = 0;
                    }
                }
                break;
            case 1:
                if (now >= start + 100) {
                    gpio_set_pin_output(gpio_b, pin, on);

                    pin += 7;
                    if (pin > 14) {
                        on  = !on;
                        pin = 0;
                    }
                    start = now;

                    ++state_counter;
                    if (state_counter >= 30) {
                        state         = 0;
                        state_counter = 0;
                    }
                }
                break;
        }
    }
}
